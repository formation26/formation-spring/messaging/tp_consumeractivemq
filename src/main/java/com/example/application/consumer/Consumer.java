package com.example.application.consumer;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;


public class Consumer implements MessageListener {

	@Override
	public void onMessage(Message message) {
		
		if (message instanceof TextMessage) {
			TextMessage tMessage = (TextMessage) message;
			try {
				System.out.println(tMessage.getText());
			} catch (JMSException e) {
				e.printStackTrace();
			}
			
		}
	}
}
