
```java
public class App {

	public static void main(String[] args) {
		ConnectionFactory factory = new ActiveMQConnectionFactory("admin", "admin", "tcp://localhost:61616");

		try {
			Connection connection = factory.createConnection();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			connection.start();
			Destination queue = session.createQueue("Queue_EPITA");
			MessageConsumer consumer = session.createConsumer(queue);
			consumer.setMessageListener(new Consumer());
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
```
Ici on instancie une Connection à partir usine qui fabrique des objets de type Connection que l'on va démarrer cela a pour effet de rendre la connexion active indéfiniment. A partir de cet objet nous allons récupérer une Session qui va permettre de créer une file d'attente qui se nomme 'Queue_EPITA', la session permet également de créer un consommateur de message et d'y attacher une listener c'est à dire que le consommateur va être à l'écoute des évènements qui auront lieux sur la file d'attente. Pour se faire JMS propose la méthode `setMessageListener` qui attend un objet de type `MessageListener` qui est une interface. *pour plus d'information vous pouvez consulter  la javadoc* [voir javadoc](https://docs.oracle.com/javaee/7/api/javax/jms/MessageConsumer.html)

Il va donc falloir lui fournir une implémentation de cette interface (MessageListener).

```java
package com.example.application.consumer;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;


public class Consumer implements MessageListener {

	@Override
	public void onMessage(Message message) {
		
		if (message instanceof TextMessage) {
			TextMessage tMessage = (TextMessage) message;
			try {
				System.out.println(tMessage.getText());
			} catch (JMSException e) {
				e.printStackTrace();
			}
			
		}
	}
}

```

